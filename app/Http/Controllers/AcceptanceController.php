<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AcceptanceController extends Controller
{
    public function paymentAcceptanceEmulator(Request $request)
    {
        if ($request->get('sig') == md5('bigsecret')) {
            $data = json_decode($request->getContent(), true);

            DB::table('user_wallet')->insert([
                'id' => $data['id'],
                'user_id' => $data['order_number'],
                'sum' => $data['sum'] + (($data['sum'] * $data['commission']) / 100)
            ]);
        }
    }
}
