<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class SystemController extends Controller
{
    public function paymentSystemEmulator()
    {
        $requests = [];

        for ($i = 0; $i <= rand(1, 9); $i++) {
            $requests[] = [
                'id' => $this->unique_id(10),
                'sum' => rand(10, 500),
                'commission' => (round(rand(50, 200) / 10) / 10),
                'order_number' => rand(1, 20)
            ];
        }

        $sig = md5('bigsecret');

        Http::post('http://192.168.0.106:8100/api/payment/acceptance/' . $sig, $requests);

        return response()->json($requests);
    }

    private function unique_id(int $limit)
    {
        return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, $limit);
    }
}
